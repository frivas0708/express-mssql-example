CREATE PROCEDURE todo.sp_get_tasks(@p_limit int = 100, @p_offset int = 0, @out_total int = NULL OUTPUT)
  AS BEGIN
  SELECT
    id,
    title,
    completed,
    created_at,
    updated_at
  FROM
    todo.tasks
  ORDER BY
    id OFFSET @p_offset ROWS FETCH NEXT @p_limit ROWS ONLY;
    SELECT
      @out_total = COUNT(*)
    FROM
      tasks;
END;
