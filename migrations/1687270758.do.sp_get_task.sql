CREATE PROCEDURE todo.sp_get_task(@p_id bigint)
  AS BEGIN
  SELECT
    id,
    title,
    completed,
    created_at,
    updated_at
  FROM
    todo.tasks
  WHERE
    id = @p_id;
END;
