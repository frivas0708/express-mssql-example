CREATE PROCEDURE todo.sp_update_task(@p_id bigint, @p_title varchar(MAX) = NULL, @p_completed bit = NULL)
  AS BEGIN
  UPDATE
    todo.tasks
  SET
    title = ISNULL(@p_title,
      title),
    completed = ISNULL(@p_completed,
      completed),
    updated_at = getdate() OUTPUT INSERTED.*
  WHERE
    id = @p_id;
END;
