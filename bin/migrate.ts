#!/usr/bin/env tsx
import path from 'node:path';

import Postgrator from 'postgrator';
import sql from 'mssql';

async function main() {
  const dbConfig: sql.config = {
    user: 'sa',
    password: '5DHeMDfwVN',
    server: 'localhost',
    port: 1433,
    database: 'master',
    options: {
      encrypt: false,
      trustServerCertificate: true,
      enableArithAbort: true,
    },
    requestTimeout: 15000,
    connectionTimeout: 15000,
    pool: {
      max: 1,
      min: 1,
    },
  };
  const pool = await sql.connect(dbConfig);
  const postgrator = new Postgrator({
    migrationPattern: path.resolve(process.cwd(), 'migrations', '*'),
    driver: 'mssql',
    database: 'master',
    async execQuery(query) {
      const result = await pool.batch(query);

      return { rows: result.recordset };
    },
  });

  await postgrator.migrate();

  await pool.close();
}

main().catch((error) => {
  console.error(error);
  process.exit(1);
});
