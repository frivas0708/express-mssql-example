import mssql from 'mssql';

type Page = {
  limit: number;
  offset: number;
};

type Update = Partial<{ title: string; completed: boolean }>;

export class TaskService {
  #pool: mssql.ConnectionPool;

  constructor(pool: mssql.ConnectionPool) {
    this.#pool = pool;
  }

  async create(title: string) {
    const request = this.#pool.request();

    const result = await request
      .input('p_title', mssql.VarChar(mssql.MAX), title)
      .execute('todo.sp_create_task');
    const [task] = result.recordset;

    return task;
  }

  async list({ limit = 100, offset = 0 }: Partial<Page> = {}) {
    const request = this.#pool.request();
    const result = await request
      .input('p_limit', mssql.Int, limit)
      .input('p_offset', mssql.Int, offset)
      .output('out_total', mssql.Int)
      .execute('todo.sp_get_tasks');

    return [result.recordset, result.output.out_total];
  }

  async get(id: bigint) {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .execute('todo.sp_get_task');
    const [task] = result.recordset;

    return task;
  }

  async update(id: bigint, change: Update) {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .input('p_title', mssql.VarChar(mssql.MAX), change.title)
      .input('p_completed', mssql.Bit, change.completed)
      .execute('todo.sp_update_task');
    const [task] = result.recordset;

    return task;
  }

  async delete(id: bigint) {
    const request = this.#pool.request();
    const result = await request
      .input('p_id', mssql.BigInt, id)
      .execute('todo.sp_delete_task');
    const [task] = result.recordset;

    return task;
  }
}
