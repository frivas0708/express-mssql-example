import type { Handler } from 'express';
import createHttpError from 'http-errors';

import { TaskService } from '../services/task.service.js';

export const create: Handler = async (request, response) => {
  const service = new TaskService(response.app.locals.db);
  const task = await service.create(request.body.title);

  response.status(201).json(task);
};

export const list: Handler = async (request, response) => {
  const service = new TaskService(response.app.locals.db);
  const itemsPerPage = Number(request.query.limit);
  const currentPage = Number(request.query.page);
  const [tasks, totalItems] = await service.list({
    offset: request.offset,
    limit: itemsPerPage,
  });
  const itemCount = tasks.length;
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  response.json({
    data: tasks,
    meta: {
      itemCount,
      totalItems,
      itemsPerPage,
      totalPages,
      currentPage,
    },
  });
};

export const get: Handler = async (request, response) => {
  const service = new TaskService(response.app.locals.db);
  const task = await service.get(BigInt(request.params.id));

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(task);
};

export const update: Handler = async (request, response) => {
  const service = new TaskService(response.app.locals.db);
  const task = await service.update(BigInt(request.params.id), request.body);

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(task);
};

export const remove: Handler = async (request, response) => {
  const service = new TaskService(response.app.locals.db);
  const task = await service.delete(BigInt(request.params.id));

  if (!task) {
    throw createHttpError.NotFound(
      `Task not found with id ${request.params.id}`,
    );
  }

  response.json(task);
};
