import assert from 'node:assert/strict';
import { describe, it } from 'node:test';

describe('Task routes', () => {
  it('create a new task', async () => {
    const response = await fetch('http://localhost:1337/tasks', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title: 'test' }),
    });
    const task = await response.json();

    assert.equal(response.status, 201, 'status code is 201 Created');
    assert.equal(task.title, 'test', 'task title is test');
    assert.equal(task.completed, false, 'task is not completed');
  });

  it('list all tasks', async () => {
    const response = await fetch('http://localhost:1337/tasks');
    const page = await response.json();

    assert.equal(response.status, 200, 'status code is 200 OK');
    assert.ok(Array.isArray(page.data), 'response body is an array');
    assert.ok('meta' in page, 'response body has a meta property');
  });

  it('get a task by id', async () => {
    const response = await fetch('http://localhost:1337/tasks/1');
    const task = await response.json();

    assert.equal(response.status, 200, 'status code is 200 OK');
    assert.equal(task.id, '1', 'task id is 1');
  });

  it('update a task by id', async () => {
    const response = await fetch('http://localhost:1337/tasks/1', {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title: 'changed', completed: true }),
    });
    const task = await response.json();

    assert.equal(response.status, 200, 'status code is 200 OK');
    assert.equal(task.id, '1', 'task id is 1');
    assert.equal(task.title, 'changed', 'task title is changed');
    assert.equal(task.completed, true, 'task is completed');
  });

  it('delete a task by id', async () => {
    const task = await fetch('http://localhost:1337/tasks', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ title: 'delete me' }),
    }).then((response) => response.json());
    const response = await fetch(`http://localhost:1337/tasks/${task.id}`, {
      method: 'DELETE',
    });

    assert.equal(response.status, 200, 'status code is 200 OK');
  });
});
