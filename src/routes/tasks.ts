import { Router } from 'express';
import asyncHandler from 'express-async-handler';

import * as taskController from '../controllers/task.controller.js';

const router = Router();

router
  .route('/')
  .post(asyncHandler(taskController.create))
  .get(asyncHandler(taskController.list));

router
  .route('/:id')
  .get(asyncHandler(taskController.get))
  .patch(asyncHandler(taskController.update))
  .delete(asyncHandler(taskController.remove));

export default router;
