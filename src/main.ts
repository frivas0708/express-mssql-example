import type { Server } from 'node:http';
import type { AddressInfo } from 'node:net';

import express from 'express';
import asyncHandler from 'express-async-handler';
import paginate from 'express-paginate';
import createHttpError from 'http-errors';
import mssql from 'mssql';

import taskRoutes from './routes/tasks.js';

const app = express();
const dbConfig: mssql.config = {
  user: 'sa',
  password: '5DHeMDfwVN',
  server: 'localhost',
  port: 1433,
  database: 'master',
  options: {
    encrypt: false,
    trustServerCertificate: true,
    enableArithAbort: true,
  },
};

app.use(
  express.json(),
  express.urlencoded({ extended: true }),
  paginate.middleware(10, 100),
);
app.get(
  '/health-check',
  asyncHandler(async (_, response) => {
    const result = await response.app.locals.db
      .query`SELECT CAST(1 AS bit) AS [ok]`;

    response.json({ db: result.recordset[0] });
  }),
);
app.use('/tasks', taskRoutes);
app.use(function notFoundHandler(request, _response, next) {
  next(createHttpError.NotFound(`Cannot ${request.method} ${request.url}`));
});
app.use(function errorHandler(
  error: unknown,
  _request: express.Request,
  response: express.Response,
  next: express.NextFunction,
) {
  console.error('(ノಠ益ಠ)ノ彡┻━┻', error);

  if (!error) {
    next();
    return;
  }

  if (createHttpError.isHttpError(error)) {
    error.expose = error.status < 500;
    response.status(error.statusCode).json(error);
    return;
  }

  response.status(501).json({
    statusCode: 501,
    message: error instanceof Error ? error.message : String(error),
  });
});

declare global {
  namespace Express {
    interface Locals {
      db: mssql.ConnectionPool;
    }
  }
}

(async () => {
  function handleExceptions(error: unknown) {
    console.error('(x_x)', error);
    process.exitCode = 1;
  }

  function handleServerExit(signal: string, server: Server) {
    return () => {
      console.info(`${signal} received! shutting down`);
      void db.close();

      server.close(() => {
        process.exitCode = 0;
      });
    };
  }

  const db = await mssql.connect(dbConfig);
  const server = app.listen(process.env.PORT ?? 1337, () => {
    const address = server.address() as AddressInfo;

    console.info(`Server is listening on http://localhost:${address.port}`);
  });

  app.locals.db = db;

  server.on('error', handleExceptions);

  process.on('unhandledRejection', handleExceptions);
  process.on('uncaughtException', handleExceptions);
  process.on('SIGINT', handleServerExit('SIGINT', server));
  process.on('SIGTERM', handleServerExit('SIGTERM', server));
})();
